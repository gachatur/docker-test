<% response.setContentType("text/xml"); %>
<% response.setCharacterEncoding("UTF-8"); %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<tns:IsAuthenticatedResponse xmlns:tns="http://tempuri.org/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://tempuri.org/ ../../../WebServicesRouter/WebContent/xsd/SECOTools/SOS-SSO-AuthenticationServices.xsd ">
  <authenticated><c:out value="${WCParam.validUser}"/></authenticated>
</tns:IsAuthenticatedResponse>
