version: "2.1"
services:

#########################################################################################################
# The following section is for the transaction Docker image                                             
#image: The URL and tag to the transaction Docker image.                                                        
#hostname: A name for the transaction server container.                                                 
#ports: Ports exported by transaction server. Port 5443 is SSL port.                                   
#LICENSE: Specify 'LICENSE=accept' to accept the terms and agreements and gain access to download 
#          the necessary Docker images.                                                                 
#TZ: The time zone.       
#adminPassword: The password for user configadmin, which is used to access the WebSphere Application Server Administrative Console  
#  If you omit adminPassword, then the default password is a random string                                                                                
#depends_on: The app container depends on the db container. The db container must be running                   
#            for the app container to start.     
#MERCHANTKEY_ENCRYPT: The encrypted merchant key that you created when you loaded the database schema. 
#   Merchant key is needed to decrypt the data read from the database.                                                      
#########################################################################################################
  app:
    image: ts-app:sos
    hostname: app
    environment:
      - LICENSE=accept
      - adminPassword=passw0rd    
    ports:
      - 5080:5080
      - 5443:5443
      - 9060:9060
      - 9043:9043
    networks:
      default:
        aliases:
          - app
    healthcheck:
      test: exit 0
      
##################################################################################################################################
# The following section is for the web Docker image                                                                              
#image: The URL and tag to the web server Docker image.                                                                                  
#hostname: A name for the web server container.                                                                                  
#ports: Ports exported by web server. All ports are SSL ports.                                                                   
#LICENSE: Specify 'LICENSE=accept' to accept the terms and agreements and gain access to use the Docker images.   
#TZ: The time zone.                                                                                                              
#depends_on: The web container depends on the app container. The app container must be running for the web container to start.   
##################################################################################################################################
  web:
    image: ts-web:sos
    hostname: web
    environment:
      - LICENSE=accept
    ports:
      - 80:80
      - 443:443
      - 8000-8002:8000-8002
      - 8004:8004
    depends_on:
      app:
        condition: service_healthy
    healthcheck:
      test: exit 0
   

#########################################################################################################
# The following section is for the search Docker image                                                  
#image: The URL and tag to the search Docker image.                                                             
#hostname: A name for the search server container.                                                      
#ports: Ports exported by search server. Port 3738 is SSL port.                                         
#environment: Specify the type of search node for this search container. Valid values are:              
# A) For search master:                                                                                
#             - SOLR_MASTER="True"                                                                      
#             - SOLR_SLAVE=false                                                                        
# B) For search repeater:                                                                               
#             - SOLR_MASTER="True"                                                                      
#             - SOLR_SLAVE="True"                                                                       
# C) For search slave:                                                                                  
#             - SOLR_MASTER=false                                                                       
#             - SOLR_SLAVE="True"                                                                       
#LICENSE: Specify 'LICENSE=accept' to accept the terms and agreements and gain access to download 
#         the necessary Docker images.                                                                  
#TZ: The time zone.                                                                                     
#depends_on: The search container depends on the db container. The db container must be running         
#            for the search container to start. 
#healthcheck: Checks the health of Search server. 
#BASE64_ENCRYPTED_SPIUSER_PASSWORD  
#    1. run the following command: echo -n "spiuser:<plain text spiuser password>" | base64
#    2. use the output value to replace <BASE64_ENCRYPTED_SPIUSER_PASSWORD>                                                     
#########################################################################################################
  search:
    image: search_app:sos
    hostname: search
    environment:
      - SOLR_MASTER=true
      - SOLR_SLAVE=false
      - WORKAREA=/search
      - LICENSE=accept
    ports:
      - 3737:3737
      - 3738:3738
    healthcheck:
      test: exit 0

#########################################################################################################
# The following section is for the Store Docker image                                                   
#image: The URL and tag to the store Docker image.                                                              
#hostname: A name for the store server container.                                                       
#ports: Ports exported by store server. Port 8443 is SSL port.                                          
#LICENSE: Specify 'LICENSE=accept' to accept the terms and agreements and gain access to          
#         download the necessary Docker images.                                                         
#TZ: The time zone.                                                                                     
#depends_on: The store container depends on the app container and search container.                     
#            The app and search containers must be running for the store container to start.            
#########################################################################################################
  store:
    image: crs_app:sos
    hostname: store
    environment:
      - LICENSE=accept
    ports:
      - 8080:8080
      - 8443:8443
    depends_on:
      app:
        condition: service_healthy
      search:
        condition: service_healthy
    healthcheck:
      test: exit 0


#################################################################################################################################
# The following section is for the utilities Docker image                                                                       
#image: The URL and tag to the utilities Docker image.                                                                                  
#hostname: A name for the utilities container.                                                                                  
#LICENSE: Specify 'LICENSE=accept' to accept the terms and agreements and gain access to use the Docker images.  
#DBHOST: The database host. 
#DBNAME: Database name.
#DBPORT: Port exported by the database server.
#DBUSER: The database user name.
#DBADMIN: The database administrator user name.                                                                            
#tty: Connects a pseudo terminal to access the utilities inside the container.                                                  
#################################################################################################################################
  utils:
    image: ts-utils:sos
    hostname: utils
    environment:
      - LICENSE=accept
    tty: true
    healthcheck:
      test: exit 0

