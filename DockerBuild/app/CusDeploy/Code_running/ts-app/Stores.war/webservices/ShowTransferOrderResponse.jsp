<% response.setContentType("text/xml"); %>
<% response.setCharacterEncoding("UTF-8"); %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<tns:TransferOrderResponse xmlns:tns="http://tempuri.org/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://tempuri.org/ ../../../WebServicesRouter/WebContent/xsd/SECOTools/SOS-SSO-OrderTransferServices.xsd ">
  <OrderTransferRes>
    <IsSuccess><c:out value="${WCParam.cartSuccess}"/></IsSuccess>
    <URL><c:out value="${WCParam.cartURL}"/></URL>
    <c:if test="${not empty WCParam.serviceResponseHeaderMsgList}">
	    <Messages>
	    	<c:forEach var="msg" items="${WCParam.serviceResponseHeaderMsgList}">
	      		<Message><c:out value="${msg}"/></Message>
	      	</c:forEach>
	    </Messages>
    </c:if>
    <c:if test="${not empty WCParam.serviceResponseMap}">
	    <Products>
	    	<c:forEach var="error" items="${WCParam.serviceResponseMap}">
	      		<Product>
	        		<ProductNumber><c:out value="${error.key}"/></ProductNumber>
	        		<IsAdded><c:out value="${error.value.isAdded}"/></IsAdded>
	        		<Messages>
	        			<c:forEach var="value" items="${error.value.errorMsgSet}"> 
	          				<Message><c:out value="${value}"/></Message>
	          			</c:forEach>
	        		</Messages>
	      		</Product>
	      	</c:forEach>
	    </Products>
    </c:if>
  </OrderTransferRes>
</tns:TransferOrderResponse>