<% response.setContentType("text/xml"); %>
<% response.setCharacterEncoding("UTF-8"); %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<tns:GetOrganizationResponse xmlns:tns="http://tempuri.org/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://tempuri.org/ ../../../WebServicesRouter/WebContent/xsd/SECOTools/SOS-SSO-OrganizationServices.xsd ">
  <OrgResponse>
    <storeId><c:out value="${WCParam.storeId}"/></storeId>
    <OrgArray>
	<c:forEach var="orgList" items="${WCParam.orgListResponse}"> 
      <OrganizationData>
        <OrgId><c:out value="${orgList.key}"/></OrgId>
        <OrgName><c:out value="${orgList.value[0]}"/></OrgName>
        <OrgDn><c:out value="${orgList.value[2]}"/></OrgDn>
      </OrganizationData>
    </c:forEach>
    </OrgArray>
  </OrgResponse>
</tns:GetOrganizationResponse>
