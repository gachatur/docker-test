<% response.setContentType("text/xml"); %>
<% response.setCharacterEncoding("UTF-8"); %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<tns:GetOrganizationResponse2 xmlns:tns="http://tempuri.org/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://tempuri.org/ ../../../WebServicesRouter/WebContent/xsd/SECOTools/SOS-SSO-OrganizationServices2.xsd ">
  <OrgResponse>
    <storeId><c:out value="${WCParam.storeId}"/></storeId>
    <OrgArray>
	<c:forEach var="orgList" items="${WCParam.orgListResponse}"> 
      <OrganizationData>
        <OrgId><c:out value="${orgList[3]}"/></OrgId>
        <OrgName><c:out value="${orgList[2]}"/></OrgName>
        <CustomerNumber><c:out value="${orgList[0]}"/></CustomerNumber>
        <Status><c:out value="${orgList[1]}"/></Status>
        <OrgDn><c:out value="${orgList[4]}"/></OrgDn>
      </OrganizationData>
    </c:forEach>
    </OrgArray>
  </OrgResponse>
</tns:GetOrganizationResponse2>